all: server2

server2.o: server2.c
	gcc -c server2.c

server2: server2.o
	gcc server2.o -o server2 -lpthread		

clean:
	rm -f server2.o server2
