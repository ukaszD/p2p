#include <stdio.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>

#include <strings.h>

#define PORT 6789
#define SERVER_ADDR "127.0.0.1"
#define BUFFER_SIZE 512
#define MAX_QUEUE_SIZE 5
#define END_SYMBOL ":q"

void communicate(int);

int main(int argc, char* argv[]){
	int sockfd, client_socketfd, newsockfd;
	struct sockaddr_in server_addres, client_addres;
	uint32_t clientaddr_len;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		printf("ERROR opening socket\n");
		exit(1);
	} else {
		printf("Socket opened succesfully\n");
	}

	server_addres.sin_family = AF_INET;
	inet_aton(SERVER_ADDR, &(server_addres.sin_addr));
	server_addres.sin_port=htons(PORT);

	int bindcode = bind(sockfd, (struct sockaddr *) &server_addres, sizeof(server_addres));
	if (bindcode < 0) {
		printf("ERROR binding to port no %i\n", PORT);
		exit(1);
	}

	listen(sockfd, MAX_QUEUE_SIZE);

	clientaddr_len = sizeof(client_addres);
	newsockfd = accept(sockfd, (struct sockaddr *) &client_addres, &clientaddr_len);
	if (newsockfd < 0) {
		printf("ERROR while accepting client\n");
		exit(1);
	}
	communicate(newsockfd);
}

void communicate(int sockfd){
	char buffer[BUFFER_SIZE];
	while (strncmp(END_SYMBOL, buffer, 2) != 0){
		bzero(buffer, BUFFER_SIZE);
		if (read(sockfd, buffer, BUFFER_SIZE - 1) < 0) {
			printf("ERROR while reading data from client\n");
			break;
		}

		if (strncmp(END_SYMBOL, buffer, 2) == 0) {
			printf("Client have closed chat\n");
			break;
		}

		printf("Message from client: %s\n", buffer);
		printf("Type your response: ");
		bzero(buffer, BUFFER_SIZE);
		scanf("%s", buffer);

		if (write(sockfd, buffer, BUFFER_SIZE - 1) < 0) {
			printf("ERROR while sending data to client\n");
			break;
		}
	}
}
