#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <zconf.h>
#include <pthread.h>

//wierset 10

#define PORT 50458
#define BUFFER_SIZE 1024

int g_socketId;
int reading_socketId;
int server_fd, client_fd;
int new_socket;
struct sockaddr_in address;
char werset[BUFFER_SIZE] = "pozegnania wiotkie jak motyl switu";
char data_buffer[BUFFER_SIZE] = {0};
int opt = 1;
int addrlen = sizeof(address);

void *sendingMethod(void* socketId) {
    int* sId = (int*)socketId;
    int counter = 0;
	if (bind(server_fd, (struct sockaddr *) &address, sizeof(address)) < 0) {
	    perror("!!!Bind error!!!\n");
	    exit(EXIT_FAILURE);
	}

	printf("=====Socket binded=======\n");

    while(1) {
    
	
		if (listen(server_fd, 3) < 0) {
		    perror("!!!Listen error!!!\n");
		    exit(EXIT_FAILURE);
		}

		printf("=====Socket listen=======\n");
		printf("=========================\n");

		
    	
    	if ((g_socketId = accept(server_fd, (struct sockaddr *) &address, (socklen_t *) &addrlen)) < 0) {
    	    perror("!!!Accept error!!!\n");
    	    exit(EXIT_FAILURE);
    	}

    	printf("Data send successfully. Message sent counter: %d\n", counter);
    	
    	if (write(g_socketId, werset, BUFFER_SIZE - 1) < 0) {
        	perror("!!!Sending error!!!\n");
 	    	close(g_socketId);
    	}
    	
    	close(g_socketId);
    	counter++;
    }
}

void *scaningMethod(void* socketId) {
    int* sId = (int*)socketId;
    int counter = 0;
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT);
   
   while(1) {
	   
     for(int i=2; i<=250; i++) {
	char stringAddr[20];	
	sprintf(stringAddr,"192.168.102.%d",i);
	inet_aton(stringAddr, &(addr.sin_addr));
	 
	printf("Adress %s\n", stringAddr);
	if (reading_socketId = connect(client_fd, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
		continue;
		perror("!!!Connect error!!!\n");
	}

	if (read(client_fd, data_buffer, BUFFER_SIZE - 1) < 0) {
		perror("!!!Reading error!!!\n");
		continue;
	}

	printf("Readed data: %s\n", data_buffer);

	printf("Data read successfully. Message read counter: %d\n", counter);

	bzero(data_buffer, BUFFER_SIZE);
	close(reading_socketId);
	counter++;
     }
   }
}


void exitHandle() {
	 
}

int main() {
    atexit(exitHandle);

    printf("Server started...\n");
    printf("Waiting for connection...\n");
    printf("=========================\n");

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("!!!Socket error!!!\n");
        exit(EXIT_FAILURE);
    }

    printf("=====Socket server created======\n");
    

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("!!!Set socket error!!!\n");
        exit(EXIT_FAILURE);
    }
    
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);
    
    
    
    
    if ((client_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("!!!Socket error!!!\n");
        exit(EXIT_FAILURE);
    }

    printf("=====Socket client created======\n");
    

    if (setsockopt(client_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("!!!Set socket error!!!\n");
        exit(EXIT_FAILURE);
    }
    
    

	
    pthread_t sendingThreadId;

    if(pthread_create(&sendingThreadId, NULL, sendingMethod, &g_socketId)){
        printf("Error creating thread\n");
        close(server_fd);
        return 0;
    }
   
    pthread_t scaningThreadId;

    if(pthread_create(&scaningThreadId, NULL, scaningMethod, &reading_socketId)){
        printf("Error creating thread\n");
        close(server_fd);
        return 0;
    }
    
    
   


/*
    if (read(g_socketId, data_buffer, BUFFER_SIZE - 1) < 0) {
            perror("!!!Reading error!!!\n");
            close(g_socketId);
    }

    printf("[Client]: %s\n", data_buffer);

    bzero(data_buffer, BUFFER_SIZE);
    printf("> ");
    scanf("%s", data_buffer);

    if (write(g_socketId, data_buffer, BUFFER_SIZE - 1) < 0) {
        perror("!!!Sending error!!!\n");
 	    close(g_socketId);
    }

    do {
        bzero(data_buffer, BUFFER_SIZE);
        if (read(g_socketId, data_buffer, BUFFER_SIZE - 1) < 0) {
            perror("!!!Reading error!!!\n");
            close(g_socketId);
            break;
        }

        if (strncmp("exit", data_buffer, 2) == 0) {
            perror("!!!Client disconnected!!!\n");
	        close(g_socketId);
            break;
        }


        printf("[Client]: %s\n", data_buffer);
        bzero(data_buffer, BUFFER_SIZE);
        printf("> ");
        scanf("%s", data_buffer);

        if (write(new_socket, data_buffer, BUFFER_SIZE - 1) < 0) {
            perror("!!!Sending error!!!\n");
 	        close(g_socketId);
            break;
        }
    }
    while (strncmp("exit", data_buffer, 2) != 0);
*/
    

    pthread_join(scaningThreadId, NULL);
    pthread_join(sendingThreadId, NULL);  
    close(server_fd);
    close(client_fd);
    
    return 0;
}

